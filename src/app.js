const { Template } = require('ejs');
const express = require('express')
const app = express();
const ejs = require('ejs');
const path = require('path');
const PORT = process.env.PORT||3000;

const staticPath = path.join(__dirname, '../public');
const template_path = path.join(__dirname, '../templates/views');
//public static path
app.use(express.static(staticPath))
app.set('view engine' ,'ejs')
app.set ('views', template_path);

//routing
app.get('/',(req,res)=>{
res.render('index')
});

app.get('/about',(req,res)=>{
    res.render('about')
    });
    
app.get('/weather',(req,res)=>{
    res.render('weather')
    });
    


app.get('*',(req,res)=>{
    res.render('OPPS 404 ERROR PAGE')
    });
    

app.listen(PORT, ()=>{
    console.log(`Listing to the ${PORT}.`);
})