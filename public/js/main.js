//const { Template } = require("ejs");

//const { response } = require("express");
const submitBtn = document.querySelector('#submitBtn');
const city_name = document.querySelector('#city_name');
const cityName = document.querySelector('#cityName');
const temp = document.querySelector('#temp');
const temp_status = document.querySelector('#temp_status');

const getInfo = async(event) =>{
  event.preventDefault();
  let cityVal =cityName.value;
   if(cityVal === ""){
     city_name.innerText = "Please Enter Your City";
   }
   else{
       try{
        let URL = `https://api.openweathermap.org/data/2.5/weather?q=${cityVal}&appid=bfd0a6c9bd396932d0901b721c59605a`;
        const response = await fetch(URL);
        const data = await response.json();
        const arrData = [data];
        city_name.innerText = `${arrData[0].name},${arrData[0].sys.country}`;
        temp.innerText = arrData[0].main.temp;
        //temp_status.innerText = arrData[0].weather[0].main;
        const tempMood = arrData[0].weather[0].main;
        if(tempMood === 'clear'){
            temp_status.innerHTML ="Sunny <i class='fas fa-sun'></i>";
        }
        else if (tempMood === 'Clouds') {
            temp_status.innerHTML = " Cloud <i class='fas fa-cloud-meatball'></i>";
        }
        else if (tempMood === 'Rain') {
            temp_status.innerHTML = "Rain <i class='fas fa-cloud-showers-heavy'></i>";
        }
        else{
            temp_status.innerHTML = "Sunny <i class='fas fa-sun'></i>";
        }
       }
       catch{
        city_name.innerText = "City Name Not Found";
       }
}
}
submitBtn.addEventListener('click',getInfo);